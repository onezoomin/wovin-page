# Storage

:::info
**TBD** - this is a first overview
:::

We want to be able to support any remote storage format, but for now we started with:
- IndexDB (local storage)
- Local File system (export/sync [CAR files](https://ipld.io/specs/transport/car/))

Remote:
- [CAR files](https://ipld.io/specs/transport/car/)
- IPFS/Kubo via [**ucan-store-proxy**](https://gitlab.com/wovin/deploy/ucan-store-proxy)
  - self-hosted or join one from a friend ()
  - use `https://demo-storage.note3.app`
- ~~web3.storage (hosting IPLD blocks)~~ (deprecated/inactive)

## Planned & envisioned:
- Self-hosted nodes
- Integration with Kubo/Iroh/Homestar (the latter also for stream processing)
- Filecoin
- Nextcloud (?)
- TBD
