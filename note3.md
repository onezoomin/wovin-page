# Note3

Our main proof of concept:  
A local-first outliner app, inspired by Logseq & Workflowy

Open: **[note3.app](https://note3.app)**

[Source code](https://gitlab.com/onezoomin/wovin/-/tree/main/apps/note3)

![Screenshot](/screenshot-note3.png)
