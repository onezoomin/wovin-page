---
# outline: deep
---

# Code examples

Typecell is an in-browser JS notebook we chose to demonstrate simple usage of wovin.

:::info
**TBD** - More examples to come
:::

## Minimal example

```ts
import { retrievePubStream } from "@wovin/connect-nftstorage";
import { query } from "@wovin/core";

const { cid, dataStream } = await retrievePubStream("k51qzi5uqu5dkuhqguzpbzbx3w4ihnxiwjmra1t57k0vd3l1zt615e5pzcaho8");

const moviesQuery = query(dataStream, [
  { en: "?movieID", at: "movie/year", vl: 1987 },
  { en: "?movieID", at: "movie/director", vl: "?directorID" },
  { en: "?directorID", at: "person/name", vl: "?directorName" },
])
console.log(moviesQuery.records);
```

[Open in Typecell](https://www.typecell.org/@wovin/public/wovin-minimal-example~dkfPqcm9zmygt)

<!-- [Open in new tab](https://starboard.gg/nb/nuP3YQ5)
<iframe src="https://tennox.starboard.host/v1/embed/0.15.3/ck5blsi23akg00do50fg/nuP3YQ5/" frameborder="0" style="width: 100%;min-height: 600px; border: 1px solid purple; padding: 2px"></iframe> -->

## Basic queries & explanation

https://www.typecell.org/@wovin/public/wovin-basic-queries~dKyH2GyQ4xQMm