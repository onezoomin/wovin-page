# (Open) Source Code

Open Data needs Open Source, here's our code:

## [Monorepo](https://gitlab.com/onezoomin/wovin)
Contains wovin packages:
- @wovin/core
- @wovin/connect-web3storage
- @wovin/connect-nftstorage

and our Proof-of-Concept apps:
- [Note3](/note3)

