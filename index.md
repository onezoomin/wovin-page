---
# https://vitepress.dev/reference/default-theme-home-page
layout: home
title: Homepage

hero:
  name: "Wovin"
  # text: ""
  image:
    src: '/wovin_logo_purple.svg'
    alt: Logo
  tagline: local-first data-stream weaving engine
  actions:
    - theme: brand
      text: (Open) Source Code
      link: /source-code
    - theme: alt
      text: Quick demo
      link: /examples

features:
  - title: Wovin - data(base) engine
    link: /engine
    details: "This is the heart of our efforts, a datalog-style, pluggable local-first engine to weave data of various peers, apps and sources" 
  - title: Note3 - our PoC app
    link: /note3
    details: A PKM/note-taking app built on wovin, inspired by Logseq & Workflowy
  - title: Opencollective
    details: Help fund our effors collectively
    link: https://opencollective.com/wovin
---

