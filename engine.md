# Wovin engine

:::info
**TBD** - this is a first overview
:::

Our attempt at a way to deal with data - aligned with these ideas:

- **local-first** - app & data should work autonomously. Independent of servers & platforms.
    - **peer-to-peer** - users & services should work together as simple and direct as possible
    - **share at will** - share only the data you want, only with whom you want to
- **flexible**
  - different apps have different needs
  - even users using the same app might have differing requirements & style
  - transport data over various channels, store where & how you want to
- **hackable** - the engine we develop will never suffice the needs & wants of everyone else
  - **extensible** - make it easy to add new data sources, formats & transformations
  - **agnostic** - make it easy for other apps to interpret & create data that can weave with others
  - **open source** - open data needs open source, everyone can customize & extend
  - it should be very easy to query & transform data
  - [**end-user programmable**](https://www.inkandswitch.com/end-user-programming/)

### How?

- **datalog-style**
  - Extended EAV triple - a single data point is composed of Entity ID, Attribute name, Value, Agent ID, Timestamp and previous atom
  - this enables the most flexible view on data where different apps & users care about / contribute different attributes but can talk about / interact with the same entity
  - easily merge, split & transform data as needed
- **IPFS / IPLD** - we (mainly) care about what data we want, not where from. Also, we want to verify & cache easily.
- **end-to-end encryption** - if wanted, encrypt your data streams for intended recipients
- **In browser** - most of the internet happens in browsers, so this is our first target


### Tech-stack

Our [current implementation](./source-code) is
- alpha-stage
- Typescript-based
- using **mobx** for reactivity
  - all queries are instantly reactive to changes in the source streams - locally (we don't have realtime sync yet, only push/pull)
  - queries are broken down into computed functions, enabling other queries to re-use shared parts of the query (behaving like indices / materialized views) (**TBD**)
- **IndexDB** for local-first storage
- Web3.storage/NFT.storage as an easy first way to store data on IPFS.  
  ([more about storage](./storage))

### Usage
For now, we don't have docs, just some [examples](./examples).